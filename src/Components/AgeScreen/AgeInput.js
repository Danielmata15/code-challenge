import React from 'react';
import { connect } from 'react-redux';
import { View, Text, KeyboardAvoidingView, StyleSheet } from 'react-native';

import { Routes } from '../../Constants';
import userInfoActions from '../../Store/UserInfo/userInfo.action';

import BackArrow from '../Common/BackArrow';
import Input from '../Common/Input';
import OnBoardingButton from '../Common/onBoardingButton';
import ErrorMessage from '../Common/ErrorMessage';

class AgeInput extends React.Component {
  state = {
    age: null,
  };
  
  onChangeAge = (text) => this.setState({ age: text });

  saveAge = () => {
    this.props.saveAge(parseInt(this.state.age));
    return this.props.navigation.navigate(Routes.HeightScreen);
  }

  render() {
    const { age } = this.state;
    return (
      <View style={{ backgroundColor: 'white' }}>
        <View style={{ height: 3, backgroundColor: '#07DA8A', width: `${75}%` }} />
        <KeyboardAvoidingView behavior='padding' style={styles.container}>
          <Text style={styles.titleText}>How old are you?</Text>
          <Input type={'numeric'} width='90%' value={age} onChange={this.onChangeAge} aligment='center' />
          <ErrorMessage display={age && (age < 13 || age > 120)} message='Please enter a valid age' />
          <OnBoardingButton title='continue' onPress={this.saveAge} disabled={!age} />
          {/* this bit of ugly is so the KeyboardAvoidingView works with the last element */}
          <View style={{ height: 20 }} />
        </KeyboardAvoidingView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    height: '100%',
    alignItems: 'center',
    justifyContent: 'space-between',
    backgroundColor: 'white',
    paddingTop: 26,
  },
  titleText: {
    fontFamily: 'FiraSans-Bold',
    fontSize: 24,
    color: '#292C30',
  },
});

AgeInput.navigationOptions = ({ navigation }) => ({
  // headerBackImage: (<Image source={backArrowIcon} />)
  headerLeft: (<BackArrow navigation={navigation} />),
});

const actions = {
  saveAge: userInfoActions.setAge,
};

export default connect(null, actions)(AgeInput);
