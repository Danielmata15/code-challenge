import React from 'react';
import { View, StyleSheet, Text } from 'react-native';
import PropTypes from 'prop-types';

const Information = ({ name, value, isLast }) => {
  const styles = createStyles(isLast);
  return (
    <View style={styles.container}>
      <Text style={styles.nameText}>{name}</Text>
      <Text style={styles.valueText}>{value}</Text>
    </View>
  );
};

Information.propTypes = {
  name: PropTypes.string.isRequired,
  vlaue: PropTypes.string,
  isLast: PropTypes.bool,
}

const createStyles = isLast => StyleSheet.create({
  container: {
    borderBottomColor: '#F4F4F4',
    borderBottomWidth: isLast ? 0 : 1,
    flexDirection: 'row',
    paddingVertical: 20,
    justifyContent: 'space-between',
    paddingRight: 15,
  },
  nameText: {
    fontFamily: 'FiraSans-Regular',
    fontSize: 16,
    color: '#292C30',
  },
  valueText: {
    fontFamily: 'FiraSans-Regular',
    fontSize: 16,
    color: '#7A7D7E',
  },
})

export default Information;
