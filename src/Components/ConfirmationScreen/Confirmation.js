import React from 'react';
import { ImageBackground, View, Text, StyleSheet } from 'react-native';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import { selectors as userSelectors } from '../../Store/UserInfo/userInfo.reducer';
import { selectors as goalSelectors } from '../../Store/Goal/goal.reducer';

import BackArrow from '../Common/BackArrow';
import Information from './Information';
import OnBoardingButton from '../Common/onBoardingButton';
import AnimatedConfirmation from '../Common/ConfirmationBackground';

const Confirmation = ({ age, height, goal, navigation, unit }) => (
  <AnimatedConfirmation>
    <View style={{ justifyContent: 'flex-start' }}>
      <BackArrow navigation={navigation} />
    </View>
    <View style={styles.innerContainer}>
      <Text style={styles.titleText}>Confirm your details:</Text>
      <View style={{ borderRadius: 15, backgroundColor: 'white', marginTop: 40, borderColor: '#E1E1E1', paddingLeft: 15, width: '100%', borderWidth: 1 }}>
        <Information name='Goal' value={goal} />
        <Information name='Age' value={age} />
        <Information
          name='Height'
          value={unit === 'cm' ? `${height}cm` : `${height}ft` }
          isLast
        />
      </View>
      <View style={{marginTop: 'auto'}}>
        <OnBoardingButton title='Save' onPress={()=> {}} />
      </View>
    </View>
  </AnimatedConfirmation>
);

Confirmation.propTypes = {
  
};

const styles = StyleSheet.create({
  container: {
    width: '100%',
    height: '100%',
    backgroundColor: '#F4F4F4',
    paddingTop: 30,
  },
  innerContainer: {
    width: '100%',
    height: '100%',
    backgroundColor: 'transparent',
    alignItems: 'center',
    paddingVertical: 40,
    paddingHorizontal: 20,
  },
  titleText: {
    fontSize: 24,
    fontFamily: 'FiraSans-Bold',
  },
});

const mapStateToProps = state => ({
  age: userSelectors.getAge(state),
  height: userSelectors.getHeight(state),
  goal: goalSelectors.getSelectedGoal(state),
  unit: userSelectors.getHeightUnit(state)
});

export default connect(mapStateToProps)(Confirmation);
