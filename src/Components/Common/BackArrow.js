import React from 'react';
import { Image, StyleSheet } from 'react-native';
import backArrowIcon from '../../../Assets/icArrowLeft.png';
import BaseTouchable from './BaseTouchable';

const BackArrow = ({ navigation }) => (
  <BaseTouchable style={styles.main} onPress={() => navigation.goBack()}>
    <Image source={backArrowIcon} />
  </BaseTouchable>
)

const styles = StyleSheet.create({
  main: {
    marginLeft: 25,
  },
});

export default BackArrow;
