import React from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, Text } from 'react-native';
import BaseTouchable from "./BaseTouchable";

const RoundButton = ({ title, onPress, color, textColor, disabled, disabledColor, disabledTextColor }) => {
  const styles = createStyleSheet(color, textColor, disabled, disabledColor, disabledTextColor);
  return (
    <BaseTouchable
      onPress={onPress}
      disabled={disabled}
      style={styles.container}
    >
      <Text style={styles.text }>{ title }</Text>
    </BaseTouchable>
  );
};

RoundButton.propTypes = {
  title: PropTypes.string.isRequired,
  onPress: PropTypes.func.isRequired,
  color: PropTypes.string.isRequired,
  textColor: PropTypes.string.isRequired,
  disabled: PropTypes.bool,
  disabledColor: PropTypes.string.isRequired,
  disabledTextColor: PropTypes.string,
}
const createStyleSheet = (color, textColor, disabled, disabledColor, disabledTextColor) => {
  return StyleSheet.create({
    container: {
      paddingVertical: 15,
      paddingHorizontal: 24,
      backgroundColor: disabled ? disabledColor : color,
      borderRadius: 50,
    },
    text: {
      fontFamily: 'FiraSans-Medium',
      fontSize: 16,
      color: disabled ? disabledTextColor || textColor : textColor,
    }
  });
}

export default RoundButton;
