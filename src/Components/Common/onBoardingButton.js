//This component exists to isolate the styling of the button that appears in the three screens.

import React from 'react';
import PropTypes from 'prop-types';
import RoundButton from './RoundButton';

const OnBoardingButton = ({ disabled, onPress, title }) => (
  <RoundButton
    color='#292C30'
    textColor='white'
    disabledColor='#A9ABAC'
    disabled={disabled}
    onPress={onPress}
    title={title}
  />
);

OnBoardingButton.propTypes = {
  disabled: PropTypes.bool,
  onPress: PropTypes.func.isRequired,
  title: PropTypes.string.isRequired,
};

export default OnBoardingButton;
