import React from 'react';
import { ImageBackground, StyleSheet, Image, View } from 'react-native'
import * as Animatable from 'react-native-animatable';
import background from '../../../Assets/backgroundGrain.png';
import parsley from '../../../Assets/imgParsley.png';
import beans from '../../../Assets/imgBeans.png';


class ConfirmationBackground extends React.Component {
  render(){ 
    return (
      <ImageBackground source={background} style={styles.container}>
        <Animatable.Image source={parsley} style={{ marginLeft: 'auto'}} resizeMode={'contain'} animation='fadeInRight' duration={500} />
        <Animatable.Image style={{marginRight: 'auto', bottom: 40}} source={beans} animation='fadeInLeft' duration={500} />
        <View style={{ position: 'absolute', height: '100%', width: '90%', marginBottom: 'auto', marginTop: 40 }}>
          {this.props.children}
        </View>
      </ImageBackground>
    );
  };
}
const styles = StyleSheet.create({
  container: {
    width: '100%',
    height: '100%',
    backgroundColor: '#F4F4F4',
    paddingTop: 30,
    alignItems: 'center',
  },
});

export default ConfirmationBackground;
