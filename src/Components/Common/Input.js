import React from 'react';
import PropTypes from 'prop-types';
import { TextInput, StyleSheet } from 'react-native';

const Input = ({ value, onChange, type, width, aligment }) => (
  <TextInput value={value} underlineColorAndroid='transparent' onChangeText={onChange} keyboardType={type} width={width} style={[styles.input, { textAlign: aligment }]} />
);

Input.propTypes = {
  value: PropTypes.string,
  onChanage: PropTypes.func,
  type: PropTypes.string,
  width: PropTypes.oneOf(PropTypes.string, PropTypes.number), 
  aligment: PropTypes.string,
};

Input.defaultProps = {
  aligment: 'right'
};

const styles = StyleSheet.create({
  input: {
    borderBottomColor: '#C0C0C0',
    borderBottomWidth: 1,
    fontFamily: 'FiraSans-Bold',
    fontSize: 32,
    height: 32,
  },
});

export default Input;
