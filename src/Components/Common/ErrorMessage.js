import React from 'react';
import PropTypes from 'prop-types';
import {View, Text, StyleSheet} from 'react-native';

const ErrorMessage = ({ display, message }) => ( display && <Text style={{ fontSize: 16, color: 'red', fontFamily: 'FiraSans-Regular'}}>{message}</Text>);

ErrorMessage.propTypes = {

};

export default ErrorMessage
