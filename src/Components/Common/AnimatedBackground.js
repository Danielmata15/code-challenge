import React from 'react';
import { ImageBackground, StyleSheet, Image, View } from 'react-native'
import * as Animatable from 'react-native-animatable';
import background from '../../../Assets/backgroundGrain.png';
import dumbbel from '../../../Assets/imgDumbbell.png';
import mat from '../../../Assets/imgMat.png';
import beans from '../../../Assets/imgBeans.png';


class AnimatedBackground extends React.Component {
  render(){ 
    return (
      <ImageBackground source={background} style={styles.container}>
        <Animatable.Image style={{marginRight: 'auto', width: '30%', height: '70%'}} source={beans} animation='fadeInLeft' duration={500} />
        <Animatable.View style={{ marginTop: 'auto', marginLeft: 'auto', height: '45%'}} animation='fadeInRight' duration={500}>
          <ImageBackground source={mat} style={{ width: undefined, height: undefined}} resizeMode='contain'>
            <Image source={dumbbel} style={{ marginBottom: 200}} />
          </ImageBackground>
        </Animatable.View>
        <View style={{ position: 'absolute', height: '100%', width: '90%', marginBottom: 'auto', marginTop: 40 }}>
          {this.props.children}
        </View>
      </ImageBackground>
    );
  };
}
const styles = StyleSheet.create({
  container: {
    width: '100%',
    height: '100%',
    backgroundColor: '#F4F4F4',
    paddingTop: 30,
    alignItems: 'center',
  },
});

export default AnimatedBackground;
