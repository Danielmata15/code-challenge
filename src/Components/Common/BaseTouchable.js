import { TouchableHighlight, TouchableOpacity, Platform } from 'react-native';

export default Platform.OS === 'ios' ? TouchableOpacity : TouchableHighlight;
