// If we ever want to extend the onboarding, for example adding names, we gonna need a different progress bar, so i
// created this component that while it's not used too much on the example, it would probably be used a lot more on
// a real onboarding.
import React from 'react';
import { View } from 'react-native';
import PropTypes from 'prop-types';

const ProgressBar = ({ width }) => <View style={{ height: 3, backgroundColor: '#07DA8A', width: `${width}%` }} />

ProgressBar.propTypes = {
  width: PropTypes.number.isRequired,
};

export default ProgressBar;
