import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { View, Text, KeyboardAvoidingView, StyleSheet } from 'react-native';

import { selectors } from '../../Store/UserInfo/userInfo.reducer';
import userInfoActions from '../../Store/UserInfo/userInfo.action';
import { Routes } from '../../Constants';
import BackArrow from '../Common/BackArrow';
import Input from '../Common/Input';
import ErrorMessage from '../Common/ErrorMessage';
import OnBoardingButton from '../Common/onBoardingButton';
import ProgressBar from '../Common/ProgressBar';
import UnitButton from './UnitButton';


class HeightInput extends React.Component {
  state = {
    cm: '',
    feet: '',
    in: '',
  }

  updateHeight = (number, unit) => {
    const { feet: ft, in: inches } = this.state;
    let feetAndInches;
    switch(unit) {
      case 'cm':
        let feetValue = (number / 30.48).toFixed(2);
        feetAndInches = feetValue.split('.');
        const feet = feetAndInches[0];
        const inch = feetAndInches[1];
        return this.setState({ cm: number, feet, in: inch });
      case 'ft':
       //we set value to feet in case inches doesn't exist
        feetAndInches = number;
        if (inches) {
          // add inches if they exist
          feetAndInches = `${number}.${inches}`
        };
        const cm = (feetAndInches * 30.48).toString();
        return this.setState({ cm, feet: number, in: inches});
      case 'in': {
        feetAndInches = `0.${number}`;
        if (ft) {
          feetAndInches = `${ft}.${number}`
        };
        const cm = (feetAndInches * 30.48).toFixed();
        return this.setState({ cm, feet: ft, in: number});
      }
    }

  }

  onChangeFeet = (text) => this.updateHeight(text, 'ft');

  onChangeInch = (text) => this.updateHeight(text, 'in');
 
  onChangeCm = (text) => this.updateHeight(text, 'cm');
  

  saveHeight = () => {
    this.props.saveHeight(parseInt(this.state.cm));
    return this.props.navigation.navigate(Routes.ConfirmationScreen);
  }

  renderUnit = (unit, width) => (
    <View style={{ borderBottomWidth: 1, borderBottomColor: '#C0C0C0', width: `${width}%`, paddingLeft: 10 }}>
      <Text style={styles.unitText}>{unit}</Text>
    </View>
  )

  renderCentimeterInput = () => (
    <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'flex-end'}}>            
      <Input type={'numeric'} value={this.state.cm} width='40%' onChange={this.onChangeCm} />
      {this.renderUnit('Cm', 40)}
    </View> 
  );

  renderFeetAndInchesInput = () => (
    <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'flex-end'}}>
      <Input type={'numeric'} value={this.state.feet} width='30%' onChange={this.onChangeFeet} />
      {this.renderUnit('Ft', 10)}
      <View style={{ width: '10%'}} />
      <Input type='numeric' value={this.state.in} width='30%' onChange={this.onChangeInch} />
      {this.renderUnit('In', 10)}
    </View>
  )

  shouldDisplayErrorMessage = () => {
    const { cm, feet, in: inches } = this.state;
    const feetAndInches = parseInt(`${feet}.${inches}`);
    const invalidCm = cm && cm < 125 || cm > 301;
    const invalidFt = feetAndInches < 4.10 || feetAndInches > 9.87;
    return invalidCm && invalidFt;
  }

  render() {
    const { unit, changeUnit } = this.props;
    const displayError = this.shouldDisplayErrorMessage();
    return (
      <View style={{ backgroundColor: 'white' }}>
        <ProgressBar width={100} />
        <KeyboardAvoidingView behavior='padding' style={styles.container}>
          <Text style={styles.titleText}>How tall are you?</Text>
          <View style={{ width: '100%', alignItems: 'center' }}>
            {this.props.unit === 'cm' ? this.renderCentimeterInput() : this.renderFeetAndInchesInput()}
            <View style={styles.doubleButton}>
              <UnitButton unit='FT' active={unit === 'feet'} direction='left' onPress={() => changeUnit('feet')} />
              <UnitButton unit='CM' active={unit === 'cm'} direction='right' onPress={() => changeUnit('cm')}/>
            </View>
          </View>
          <ErrorMessage display={displayError} message='please enter a valid height' />
          <OnBoardingButton title='continue' onPress={this.saveHeight} disabled={!this.state.cm} />
          {/* this bit of ugly is so the KeyboardAvoidingView works with the last element */}
          <View style={{ height: 20 }} />
        </KeyboardAvoidingView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    height: '100%',
    alignItems: 'center',
    justifyContent: 'space-between',
    backgroundColor: 'white',
    paddingTop: 26,
  },
  titleText: {
    fontFamily: 'FiraSans-Bold',
    fontSize: 24,
    color: '#292C30',
  },
  doubleButton: {
    top: 40,
    width: '60%',
    borderRadius: 50,
    flexDirection: 'row',
    borderColor: '#292C30',
    borderWidth: 1,
    justifyContent: 'space-around',
  },
  unitText: {
    color: '#A9ABAC',
    fontFamily: 'FiraSans-Regular',
    fontSize: 16,
  }
});

HeightInput.navigationOptions = ({ navigation }) => ({
  headerLeft: (<BackArrow navigation={navigation} />),
});

HeightInput.propTypes = {
  unit: PropTypes.string.isRequired,
  height: PropTypes.number,
  saveHeight: PropTypes.func.isRequired,
  changeUnit: PropTypes.func.isRequired,
};

HeightInput
const mapStateToProps = state => ({
  unit: selectors.getHeightUnit(state),
  height: selectors.getHeight(state),
});

const actions = {
  changeUnit: userInfoActions.setPreferredUnit,
  saveHeight: userInfoActions.setHeight,
}
export default connect(mapStateToProps, actions)(HeightInput);
