import React from 'react';
import PropTypes from "prop-types";
import { Text, StyleSheet } from 'react-native';
import BaseTouchable from '../Common/BaseTouchable';

const UnitButton = ({ active, unit, onPress, direction }) => {
  const styles = createStyles(direction, active);
  return (
  <BaseTouchable onPress={onPress} style={styles.container}>
    <Text style={styles.text}>{unit}</Text>
  </BaseTouchable>
  );
}

UnitButton.propTypes = {
  active: PropTypes.bool,
  onPress: PropTypes.func.isRequired,
  unit: PropTypes.oneOf('CM', 'FT'),
  direction: PropTypes.oneOf('left', 'right'),
};

const createStyles = (direction, active) => {
  const commonStyles = {
    flex: 1,
    alignItems: 'center',
    backgroundColor: active ? '#292C30' : 'white',
    paddingVertical: 10,
  }
  const rightStyles = {
    ...commonStyles,
    borderBottomRightRadius: 50,
    borderTopRightRadius: 50,
  };
  const leftStyles = {
    ...commonStyles,
    borderBottomLeftRadius: 19,
    borderTopLeftRadius: 19,
  };
  return StyleSheet.create({
    container: direction === 'right' ? rightStyles : leftStyles,
    text: {
      fontFamily: 'FiraSans-medium',
      fontSize: 16,
      color: active ? 'white' : '#292C30'
    },
  });

};

export default UnitButton;
