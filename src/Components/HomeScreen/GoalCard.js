import React from 'react';
import PropTypes from 'prop-types'
import { View, Text, StyleSheet, Image } from 'react-native';

import BaseTouchable from '../Common/BaseTouchable';

import rightIcon from '../../../Assets/chevronRight.png';

const TITLES = {
  lose_weight: 'Lose Weight',
  get_fitter: 'Get fitter',
  gain_muscle: 'Gain muscle',
};

const GoalCard = ({ value, description, selectGoal, isSelected }) => (
  <BaseTouchable
    style={[styles.card, { borderWidth: isSelected ? 1 : 0, borderColor: '#292C30'}]}
    onPress={() => selectGoal(value)}
  >
  <View style={{ flexDirection: 'row', width: '100%', justifyContent: 'space-between', alignItems: 'center'}}>
    <View>
      <Text style={styles.title}>{ TITLES[value] }</Text>
      <Text style={styles.description}>{ description }</Text>
    </View>
    <Image source={rightIcon} />
  </View>
  </BaseTouchable>
);

GoalCard.propTypes = {
  title: PropTypes.string,
  description: PropTypes.string,
  selectGoal: PropTypes.func,
  isSelected: PropTypes.bool,
};

const styles = StyleSheet.create({
  card: {
    height: 100,
    paddingVertical: 25,
    paddingLeft: 25,
    paddingRight: 15,
    marginBottom: 20,
    backgroundColor: 'white', 
    borderRadius: 8,
    borderColor: '#EBEBEB',
    shadowColor: 'black',
    elevation: 3,
    shadowOffset: { 
      width: 0,
      height: 5},
      shadowRadius: 2,
    shadowOpacity: 0.3,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center'
  },
  title: {
    fontFamily: 'FiraSans-Bold',
    fontSize: 20,
    marginBottom: 10,
  },
  description: {
    fontFamily: 'FiraSans-Regular',
    fontSize: 16,
  }
});

export default GoalCard;
