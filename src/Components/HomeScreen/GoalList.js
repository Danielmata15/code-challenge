import React from 'react';
import { connect } from 'react-redux'
import PropTypes from 'prop-types';
import { FlatList, StyleSheet } from 'react-native';
import GoalCard  from './GoalCard';
import goalAction from '../../Store/Goal/goal.action';
import { selectors } from '../../Store/Goal/goal.reducer';
import { Routes } from '../../Constants';

class GoalList extends React.Component {
  selectGoal = (value) => {
    this.props.selectGoal(value);
    this.props.navigation.navigate(Routes.AgeScreen);
  }
  
  render() {
    const { goals, selectedGoal } = this.props;
    return (
      <FlatList
        scrollEnabled={false}
        data={goals}
        keyExtractor={item => item.value}
        renderItem={({item}) => (
          <GoalCard
            id={item.value}
            value={item.value}
            description={item.description}
            selectGoal={this.selectGoal}
          />
        )}
        contentContainerStyle={{ alignContent: 'center' }}
      />
    )
  }
}

GoalList.propTypes = {
  selectGoal: PropTypes.func,
  goals: PropTypes.arrayOf(PropTypes.objectOf(PropTypes.string)),
};

const mapStateToProps = state => ({
  goals: selectors.getGoals(state),
});

const actions = {
  selectGoal: goalAction.selectGoal,
};

export default connect(mapStateToProps, actions)(GoalList);
