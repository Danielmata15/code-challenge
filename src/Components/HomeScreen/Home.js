import React from 'react';
import *  as Animatable from 'react-native-animatable';
import { Text, ImageBackground, Image, StyleSheet } from 'react-native';

import GoalList from './GoalList';

import background from '../../../Assets/backgroundGrain.png';
import logo from '../../../Assets/icon8Logo.png';
import AnimatedBackground from '../Common/AnimatedBackground';

class Home extends React.Component {

  componentDidMount() {
    this.imageRef.transitionTo({ marginTop: 0 })
  }

  render(){
    const { navigation } = this.props;
    return (
      <AnimatedBackground source={background}>
        <Animatable.Image
          source={logo}
          style={styles.image}
          ref={(image) => this.imageRef=image }
        />
        <Animatable.Text  animation='fadeInUp' style={styles.lightFont}>WELCOME TO 8FIT</Animatable.Text>
        <Animatable.View animation='fadeInUp'>
          <Text style={styles.boldFont}>What's your goal?</Text>
          <GoalList navigation={navigation} />
        </Animatable.View >
      </AnimatedBackground>
    );
  }
}
const styles = StyleSheet.create({
  lightFont: {
    fontFamily: 'FiraSans-Medium',
    fontSize: 12,
    marginBottom: 20,
    alignSelf: 'center',
  },
  boldFont: {
    fontFamily: 'FiraSans-Bold',
    fontSize: 24,
    marginBottom: 30,
    alignSelf: 'center',
  },
  image: {
    marginTop: 200,
    marginBottom: 10,
    alignSelf: 'center',
  }
});

export default Home;
