// While i'm normally not a fan of indirection like this one, i have had way too many typos with routes
// that take too long to find, so i do this now
export const Routes = {
  HomeScreen: 'home',
  AgeScreen: 'ageInput',
  HeightScreen: 'heighInput',
  ConfirmationScreen: 'confirmation',
};
