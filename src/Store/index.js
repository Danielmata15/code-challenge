import goal from './Goal/goal.reducer';
import userInfo from './UserInfo/userInfo.reducer';

export default {
  goal,
  userInfo,
};
