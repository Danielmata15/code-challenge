import { SELECT_GOAL } from "./goal.action";

const initialState = {
  goals: [
    { value: 'lose_weight', description: 'Burn fat & get lean' },
    { value: 'get_fitter', description: 'Tone up & feel healthy' },
    { value: 'gain_muscle', description: 'Build mass & strength' },
  ],
  selectedGoal: 'lose_weight',
};

const goalReducer = (state = initialState, action) => {
  switch(action.type) {
    case SELECT_GOAL:
      return { ...state, selectedGoal: action.selectedGoal };
    default:
      return state;
  }
};

// while probably not needed with this level of complexity, is just a best practice to add selectors

export const selectors = {
  getSelectedGoal: state => {
    const selectedGoal = state.goal.selectedGoal;
    const goals = state.goal.goals;
    const goalObj = goals.filter(g => g.value === selectedGoal);
    return goalObj[0].description;
  },
  getGoals: state => state.goal.goals,
};

export default goalReducer;
