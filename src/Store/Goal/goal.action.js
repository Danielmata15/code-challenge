export const SELECT_GOAL = 'user select a goal';

const selectGoal = value => ({
  type: SELECT_GOAL,
  selectedGoal: value,
});

export default {
  selectGoal,
};
