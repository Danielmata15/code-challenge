
import goalReducer, { selectors } from './goal.reducer';
import goalActions from './goal.action';

describe('goal reducer tests', () => {
  test('should return initialState with no action match', () => {
    const state = goalReducer(undefined, {});
    const expectedState = {
      goals: [
        { value: 'lose_weight', description: 'Burn fat & get lean' },
        { value: 'get_fitter', description: 'Tone up & feel healthy' },
        { value: 'gain_muscle', description: 'Build mass & strength' },
      ],
      selectedGoal: '',
    };
    expect(state).toEqual(expectedState);
  });
  test('should set selected goal on the action', () =>  {
    const actionObj = goalActions.selectGoal('lose_weight');
    const state = goalReducer(undefined, actionObj);
    const expectedState = {
      goals: [
        { value: 'lose_weight', description: 'Burn fat & get lean' },
        { value: 'get_fitter', description: 'Tone up & feel healthy' },
        { value: 'gain_muscle', description: 'Build mass & strength' },
      ],
      selectedGoal: 'lose_weight',
    };
    expect(state).toEqual(expectedState);
  });
  describe('selector tests', () => {
    const store = {
      goal: {
        goals: [
          { value: 'goal_1', description: 'goal 1' },
          { value: 'goal_2', description: 'goal 2' },
        ],
        selectedGoal: 'goal_2',
      },
    };

    test('should return goals', () => {
      const goals = selectors.getGoals(store);
      const expectedResult = [
        { value: 'goal_1', description: 'goal 1' },
        { value: 'goal_2', description: 'goal 2' },
      ];
      expect(goals).toEqual(expectedResult);
    });
    test('should return selectedGoal', () => {
      const selectedGoal = selectors.getSelectedGoal(store);
      const expectedResult = 'goal 2';
      expect(selectedGoal).toEqual(expectedResult);
    })
  });
});