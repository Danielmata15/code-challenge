import { SET_AGE, SET_HEIGHT, SET_HEIGHT_UNIT } from "./userInfo.action";

const initialState = {
  userHeight: null,
  userAge: null,
  heightUnit: 'cm',
};

const userInfoReducer = (state = initialState, action) => {
  switch(action.type) {
    case SET_HEIGHT_UNIT: {
      return { ...state, heightUnit: action.unit };
    }
    case SET_HEIGHT:
      return { ...state, userHeight: action.heightValue };
    case SET_AGE:
      return { ...state, userAge: action.ageValue };
    default:
      return state;
  }
};

export const selectors = {
  getAge: state => state.userInfo.userAge,
  getHeight: state => {
    let height = state.userInfo.userHeight;
    const unit = state.userInfo.heightUnit;
    if (unit === 'feet') {
      height /= 30.48;
      height = Math.round(height * 10) / 10;
    }
    return height;
  },
  getHeightUnit: state => state.userInfo.heightUnit,
};

export default userInfoReducer;
