export const SET_HEIGHT = 'set user height';
export const SET_AGE = 'set user age';
export const SET_HEIGHT_UNIT = 'set preferred unit';

const setHeight = heightValue => ({
  type: SET_HEIGHT,
  heightValue,
});

const setAge = ageValue => ({
  type: SET_AGE,
  ageValue,
});

const setPreferredUnit = unit => ({
  type: SET_HEIGHT_UNIT,
  unit,
})
export default {
  setHeight,
  setAge,
  setPreferredUnit,
};
