import userInfoReducer, { selectors } from './userInfo.reducer';
import userInfoActions from './userInfo.action';

describe('userReducer test', () => {
  test('should return initial state with no action', () => {
    const state = userInfoReducer(undefined, {});
    const expectedState = {
      userHeight: null,
      userAge: null,
      heightUnit: 'cm',
    };
    expect(state).toEqual(expectedState);
  });
  test('should set height value', () => {
    const action = userInfoActions.setHeight(180);
    const state = userInfoReducer(undefined, action);
    const expectedState = {
      userHeight: 180,
      userAge: null,
      heightUnit: 'cm',
    };
    expect(state).toEqual(expectedState);
  });
  test('should set age value', () => {
    const action = userInfoActions.setAge(25);
    const state = userInfoReducer(undefined, action);
    const expectedState = {
      userHeight: null,
      userAge: 25,
      heightUnit: 'cm',
    };
    expect(state).toEqual(expectedState); 
  });
  test('should set the height unit', () => {
    const action = userInfoActions.setPreferredUnit('ft');
    const state = userInfoReducer(undefined, action);
    const expectedState = {
      userHeight: null,
      userAge: null,
      heightUnit: 'ft',
    };
    expect(state).toEqual(expectedState);
  })
  describe('selector tests', () => {
    const store = {
      userInfo: {
        userHeight: 180,
        userAge: 25,
        heightUnit: 'cm',
      },
    };
    test('should get the age value', () => {
      const age = selectors.getAge(store);
      const expectedResult = 25;
      expect(age).toEqual(expectedResult);
    });
    test('should get height based on the prefence', () => {
      const height = selectors.getHeight(store);
      const expectedResultInCm = 180;
      const expectedResultInFeet = 5.90;
      expect(height).toEqual(expectedResultInCm);
      const newState = { ...store, userInfo: { ...store.userInfo, heightUnit: 'feet' }};
      const heightInFeet = selectors.getHeight(newState);
      expect(heightInFeet).toEqual(expectedResultInFeet);
    })
  });
});