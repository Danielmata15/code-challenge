import React from 'react';
import { Provider } from 'react-redux';
import { combineReducers, createStore } from 'redux';
import { createStackNavigator } from 'react-navigation';

import Home from './src/Components/HomeScreen/Home';
import HeightInput from './src/Components/HeightScreen/HeightInput';
import Age from './src/Components/AgeScreen/AgeInput';
import AnimatedBackGround from './src/Components/Common/AnimatedBackground';

import { Routes } from './src/Constants';
import reducers from './src/Store/index';
import Confirmation from './src/Components/ConfirmationScreen/Confirmation';

const store = createStore(combineReducers(reducers));

const RootStack = createStackNavigator({
  [Routes.HomeScreen]: {
    screen: Home,
    navigationOptions: {
      header: null,
    }
  },
  [Routes.HeightScreen]: HeightInput,
  [Routes.AgeScreen]: Age,
  [Routes.ConfirmationScreen]: {
    screen: Confirmation,
    navigationOptions: {
      header: null,
    },
  },
}, {
  initialRouteName: Routes.HomeScreen,
});

const App = () => (
  <Provider store={store}>
    <RootStack />
  </Provider>
);

export default App;
