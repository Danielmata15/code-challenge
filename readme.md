# 8fit Code Challenge # 

The challenge was actually a lot of fun and different from what i'm used to from this kind of things, I enjoyed the comprehensive design and the freedom with technologies. all in all i spent about 24-28 hours on it.

# Running the code #

## Pre-requisites ##
You need to have a working version of node.js installed. You can go to [their webpage](https://nodejs.org/en/) to download their intaller, but i recommend using [https://github.com/creationix/nvm](nvm) which a really powerfull node version manager, you can follow the creator instructions, it works with zero config on linux and osx but it has some issue on windows so be wary if that's your OS.

After you have nodejs installed, follow the instructions on the building projects with native code tab [on the react native docs page](https://facebook.github.io/react-native/docs/getting-started.html).

After that is done, you can just run these on your terminal.

* `npm install`
* ` npm install -g react-native-cli`

that will install the packages we need. After that, run
 * `react-native run-ios`
  
  or
 * `react-native run-android`
  
  depending on yor platform of choice and that should open the app on your simulator. After the first time, you can just use `npm start` on your terminal and it'll start.

# Technical choices #

## Folder architecture ##

This is the way i like folders organized, ordering by screen means code is easier to find and work on. I think this way when a dev is going to work on a feature, he can find all the related files easily. Likewise, pieces of code is easy and fast.

## indirection and components ##
I'm not a fan of being extremely DRY with code to the point where you have abstractions within abstractions that made refactoring and finding code a pain. I used some abstractions to isolate styles (onboardingbutton, input) where i believed it was useful.

There is one particular piece of premature optimization on the `ProgressBar` component. I thought that if this onboarding where to be expanded, the progressbar would need to be smaller for things like "name and lastname".

## state management ##

I decided to use redux becuase it is what i have more experience with and i really enjoy using it after so much time. At first i thought about using react `setState` but decided against it becuase react-navigation doesn't really goes will with it.

I enjoy redux architecture and the `redux way` to do things, also it was extremely simple to write a test suite using tdd for the redux part. I do think it's a little overkill, but the great part about redux is that is really simple to take it out of the app if needed.

## Navigation ##

For an app this simple, react-navigation its more than enough, i'm used to it and it never gets on the way. Without nested navigatiors, the api is extremely simple and i never got any issues with it, the only problem is a warning that i can get to make it go away, but i guess it's a small price to pay.

## Animations ##

Being honest, animations aren't something i'm super used to, so funny enough they were one of the hardest part for me. I decided on Animatable because it had the animations i was gonna need baked in and installing/using it was extremely simple. I tried to isolate the use to few files so changing it for another animation framework is simple and doesn't impact much of the codebase.

## If i had more time ##

I probably would have tried to do all the animations with react native `Animated` api, also, the whole `AnimationBackground` `ConfirmationBackground` is a sore spot for me.

At first i was thinking about a background component that animated passed components, but i couldn't get it to somewhere i was confortable and decided to repeat some code. This is the kind of thing i definitely would have asked opinions on and maybe pair, or hopefully get input on a code review.

I would have tried to clean up the height input logic, probably to redux where it can be easy to test and turn that component into a functional one. The error message code also needs some work, it was the last thing i did and i think it shows how rushed it was.

I also had a really weird issue with the unitButton, that's why the left radius is 19, if you go over that, it has a weird layout issue i couldn't fix, feel free to shoot a pr ;)

I would have liked to add enzyme and do ui testing, but in the end i felt i couldn't take more time and there wasn't that much logic to test on the components anyways.


